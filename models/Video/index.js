var mongoose = require('mongoose');
var Schema = mongoose.Schema;
mongoose.Promise = global.Promise;

var videoSchema = new Schema({
    'videoTitle': { type: String, Required: true },
    'videoUrl': { type: String, Required: true },
    'videoDescription': { type: String, Required: false }
});

module.exports = mongoose.model('Video', videoSchema);