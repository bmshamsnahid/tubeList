'use strict';

var express = require('express');
var router = express.Router();

var base = process.env.PWD;

var videoController = require(base + '/controllers/videoController');

router.post('/video', videoController.createVideo);
router.get('/videos', videoController.getVideos);
router.get('/video/:id', videoController.getVideo);
router.put('/video/:id', videoController.updateVideo);
router.delete('/video/:id', videoController.deleteVideo);

module.exports = router;
