

# TUBE LIST
A MEAN Stack application to list YouTube videos.

#### N.B. The server and client sides are totally separate

## Setup
- Install NodeJS v7.1.0
- `npm install -g gulp gulp-cli eslint bower`
- First run server, then run client

## Server - Express JS
- Create 2 databases via robomongo `tubeList` and `testTubeList`
- Navigate to server/
- `npm install` to install project dependencies
- `nodemon` to run the app
- `npm test` to run unit tests

## Server Side Testing
- Integration testing via PostMan
- Unit testing via Mocha.js (Assertion library Should.js)

## Server Side File Structure
```bash
.
├── app.js # My node server
├── config
│   └── index.js # Database configaration
├── controllers
│   └── videoController # My Video CRUD operation with test files
│       ├── TEST
│       │   └── runner.js
│       └── index.js
├── models
│   └── Video # Video Schema
│       └── index.js
├── public # If required further
├── routes # contains all route endpoints
│   └── endpoints
│       └── index.js
├── test # Configuration for running tests
│   └── utils.js
├── package.json
 ```

## Client - Angular JS
- User Angular-CLI Boilerplate
- Navigate to client/
- `npm install`
- `bower install`
- `ng serve` to run the app

# License
This project is licensed under the MIT license.

If you have any questions or comments, please create an issue.