

var base = process.env.PWD;
var Video = require(base + '/models/Video');

var createVideo = (req, res) => {
    var video = new Video(req.body);
    video.save((err, video) => {
        if(err) { res.send(500, err); }
        res.json(200, video);
    });
};

var getVideos = (req, res) => {
    Video.find({}, (err, videos) => {
        if(err) { res.send(500, err); }
        res.send(200, videos);
    });
};

var getVideo = (req, res) => {
    Video.findById(req.params.id, (err, video) => {
        if(err) { res.send(500, err); }
        res.json(200, video);
    });
};

var updateVideo = (req, res) => {
    Video.findById(req.params.id, (err, video) => {
        if(err) { res.send(500, err); }
        if(req.body.videoTitle) video.videoTitle = req.body.videoTitle;
        if(req.body.videoUrl) video.videoUrl = req.body.videoUrl;
        if(req.body.videoDescription) video.videoDescription = req.body.videoDescription;
        video.save((err, video) => {
            if(err) { res.send(500, err); }
            res.json(200, video);
        });
    });
}

var deleteVideo = (req, res) => {
    Video.findByIdAndRemove(req.params.id, (err, video) => {
        if(err) { res.send(500, err); }
        res.json(200, {'deleted': true});
    });
};

module.exports = {
    createVideo,
    getVideos,
    getVideo,
    updateVideo,
    deleteVideo
};
