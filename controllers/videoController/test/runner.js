'use strict';

process.env.NDOE_ENV = 'test';
var base = process.env.PWD;
var config = require(base + '/config');
var logger = require('mocha-logger');
var mongoose = require('mongoose');
var videoController = require(base + '/controllers/videoController');
var Video = require(base + '/models/Video');
var should = require('should');
var testUtils = require(base + '/test/utils');

describe('VIDEOS API TESTING', () => {
    var id, dummyVideo;

    before((done) => {
        mongoose.connect(config.dbTest, (err) => {
            if(err) { console.log('Error in connecting testing database: ' + err); }
            else { done(); }
        });

        dummyVideo = new Video({
            'videoTitle': 'This is dummy video title',
            'videoUrl': 'This is dummy video url',
            'videoDescription': 'This is dummy video description'
        });

        dummyVideo.save((err, video) => {
            if(err) { res.send(err); }
            id = video._id;
        });
    });

    describe('CREATE VIDEO', () => {
        it('should respond with an array of videos', (done) => {
            var req = {
                body: {
                    'videoTitle': 'This is test video title',
                    'videoUrl': 'This is a test video url',
                    'videoDescription': 'This is a test video description'
                }
            };
            var res = testUtils.responseValidatorAsync(200, (video) => {
                video.should.have.property('videoTitle');
                video.should.have.property('videoUrl');
                video.should.have.property('videoDescription');
                video.videoTitle.should.equal('This is test video title');
                video.videoUrl.should.equal('This is a test video url');
                video.videoDescription.should.equal('This is a test video description');

                done();
            });
            videoController.createVideo(req, res);
        });
    });

    describe('GET VIDEOS', () => {
        it('should respond with an array of video', (done) => {
            var req = {};
            var res = testUtils.responseValidatorAsync(200, (videos) => {
                videos.length.should.equal(2);
                videos[0].should.have.property('videoTitle');
                done();
            });
            videoController.getVideos(req, res);
        });
    });

    describe('GET A VIDEO', () => {
        it('should get a video', (done) => {
            var req = {
                params: {
                    id: id
                }
            };
            var res = testUtils.responseValidatorAsync(200, (video) => {
               video.videoTitle.should.equal('This is dummy video title');
               done();
            });
            videoController.getVideo(req, res);
        });

        it('should throw an error for an invalid id', (done) => {
            var req = {
                params: {
                    id: '123'
                }
            };
            var res = testUtils.responseValidatorAsync(500, (err) => {
                done();
            });
            videoController.getVideo(req, res);
        });
    });

    describe('UPDATE VIDEO', () => {
        it('it should update the existing video information', (done) => {
            var req = {
                params: { id: id},
                body: { 'videoTitle': 'Updated Video title'}
            }
            var res = testUtils.responseValidatorAsync(200, (video) => {
                video.videoTitle.should.equal('Updated Video title');
                done();
            });
            videoController.updateVideo(req, res);
        });
    });

    describe('DELETE A VIDEO', () => {
        it('should delete a video', (done) => {
           var req = {
               params: { id: id}
           };
           var res = testUtils.responseValidatorAsync(200, (obj) => {
               obj.should.have.property('deleted');
               obj.deleted.should.equal(true);
               done();
           });
           videoController.deleteVideo(req, res);
        });
    });

    after((done) => {
        Video.remove({}, (err) => {
            if(err) { console.log(err); }
        });
        mongoose.disconnect(done);
    });
});